import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../constants/app_text.dart';
import '../constants/dimensions.dart';

class DateSelectorButton extends StatefulWidget {
  const DateSelectorButton({
    super.key,
    required this.child,
    required this.selectedDate,
    this.onTodayBtnTap,
    this.onSelectBtnTap,
  });

  final Widget child;

  final DateTime selectedDate;

  final VoidCallback? onTodayBtnTap;
  final Function(DateTime)? onSelectBtnTap;

  @override
  State<DateSelectorButton> createState() => _DateSelectorButtonState();
}

class _DateSelectorButtonState extends State<DateSelectorButton> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        DateTime newDate = widget.selectedDate;

        showModalBottomSheet<void>(
          context: context,
          builder: (ctx) {
            return Padding(
              padding: const EdgeInsets.all(Dimensions.spacing12),
              child: StatefulBuilder(builder: (context, setModalState) {
                return Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    SizedBox(
                      height: 200,
                      child: CupertinoDatePicker(
                        key: UniqueKey(),
                        initialDateTime: newDate,
                        mode: CupertinoDatePickerMode.date,
                        onDateTimeChanged: (value) {
                          newDate = value;
                        },
                        dateOrder: DatePickerDateOrder.dmy,
                      ),
                    ),
                    Wrap(
                      spacing: Dimensions.spacing16,
                      runSpacing: Dimensions.spacing16,
                      children: [
                        _FilledButton(
                          onPressed: () {
                            final now = DateTime.now();

                            widget.onTodayBtnTap?.call();

                            setModalState(() {
                              newDate = now;
                            });
                          },
                          title: AppText.todayBtn,
                        ),
                        _FilledButton(
                          onPressed: () {
                            widget.onSelectBtnTap?.call(newDate);

                            Navigator.pop(ctx);
                          },
                          title: AppText.selectBtn,
                        )
                      ],
                    )
                  ],
                );
              }),
            );
          },
        );
      },
      child: widget.child,
    );
  }
}

class _FilledButton extends StatelessWidget {
  const _FilledButton({
    required this.onPressed,
    required this.title,
  });

  final VoidCallback onPressed;
  final String title;

  @override
  Widget build(BuildContext context) {
    return FilledButton(
      style: FilledButton.styleFrom(
        backgroundColor: Colors.teal,
      ),
      onPressed: onPressed,
      child: Text(title),
    );
  }
}
