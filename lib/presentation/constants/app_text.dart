import 'package:flutter/material.dart';

@immutable
class AppText {
  const AppText._();

  static const String dailyCalendar = 'Lịch ngày';
  static const String monthlyCalendar = 'Lịch tháng';
  static const String changeDate = 'Đổi ngày';
  static const String expand = 'Mở rộng';

  static const String monday = 'Thứ hai';
  static const String tuesday = 'Thứ ba';
  static const String wednesday = 'Thứ tư';
  static const String thursday = 'Thứ năm';
  static const String friday = 'Thứ sáu';
  static const String saturday = 'Thứ bảy';
  static const String sunday = 'Chủ nhật';

  static const String todayBtn = 'Hôm nay';
  static const String selectBtn = 'Lựa chọn';

  static const String lunarCalendar = 'Lịch âm';
  static const String zodiacHour = 'Giờ hoàng đạo';
  static const String departureDirection = 'Hướng xuất hành';
  static const String goodStarsBadStars = 'Sao tốt - sao xấu';
  static const String goodStars = 'Sao tốt';
  static const String badStars= 'Sao xấu';
  static const String thingsToDo= 'Việc nên làm';
  static const String thingsNotToDo= 'Việc không nên làm';

  static const String day = 'Ngày';
  static const String month = 'Tháng';
  static const String year = 'Năm';

}
