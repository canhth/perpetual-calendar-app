import 'package:flutter/material.dart';

enum SwipeDirection {
  leftToRight,
  rightToLeft,
  topToBottom,
  bottomToTop,
}

extension SwipeDirectionExtension on SwipeDirection {
  bool get isLeftToRight => this == SwipeDirection.leftToRight;
  bool get isRightToLeft => this == SwipeDirection.rightToLeft;
  bool get isTopToBottom => this == SwipeDirection.topToBottom;
  bool get isBottomToTop => this == SwipeDirection.bottomToTop;

  Offset toBeginOffset() {
    if (isRightToLeft) {
      return const Offset(1.0, 0.0);
    }
    if (isLeftToRight) {
      return const Offset(-1.0, 0.0);
    }
    if (isTopToBottom) {
      return const Offset(0.0, -1.0);
    }
    if (isBottomToTop) {
      return const Offset(0.0, 1.0);
    }

    return const Offset(0.0, 0.0);
  }
}
