import 'dart:math';

/// Documentation : https://www.informatik.uni-leipzig.de/~duc/amlich/calrules.html

class LunarSolarCalendarUtils {
  static final _singleton = LunarSolarCalendarUtils._internal();
  LunarSolarCalendarUtils._internal();

  factory LunarSolarCalendarUtils() => _singleton;

  static const canList = [
    "Canh",
    "Tân",
    "Nhâm",
    "Quý",
    "Giáp",
    "Ất",
    "Bính",
    "Đinh",
    "Mậu",
    "Kỉ",
  ];

  static const chiList = [
    "Thân",
    "Dậu",
    "Tuất",
    "Hợi",
    "Tý",
    "Sửu",
    "Dần",
    "Mão",
    "Thìn",
    "Tị",
    "Ngọ",
    "Mùi"
  ];

  static const chiForMonthList = [
    "Dần",
    "Mão",
    "Thìn",
    "Tị",
    "Ngọ",
    "Mùi",
    "Thân",
    "Dậu",
    "Tuất",
    "Hợi",
    "Tý",
    "Sửu",
  ];

  static const can = [
    'Giáp',
    'Ất',
    'Bính',
    'Đinh',
    'Mậu',
    'Kỷ',
    'Canh',
    'Tân',
    'Nhâm',
    'Quý'
  ];
  static const chi = [
    'Tý',
    'Sửu',
    'Dần',
    'Mão',
    'Thìn',
    'Tỵ',
    'Ngọ',
    'Mùi',
    'Thân',
    'Dậu',
    'Tuất',
    'Hợi'
  ];
  static const tietKhi = [
    'Xuân phân',
    'Thanh minh',
    'Cốc vũ',
    'Lập hạ',
    'Tiểu mãn',
    'Mang chủng',
    'Hạ chí',
    'Tiểu thử',
    'Đại thử',
    'Lập thu',
    'Xử thử',
    'Bạch lộ',
    'Thu phân',
    'Hàn lộ',
    'Sương giáng',
    'Lập đông',
    'Tiểu tuyết',
    'Đại tuyết',
    'Đông chí',
    'Tiểu hàn',
    'Đại hàn',
    'Lập xuân',
    'Vũ thủy',
    'Kinh trập'
  ];
  static const gioHoangDao = [
    '110100101100',
    '001101001011',
    '110011010010',
    '101100110100',
    '001011001101',
    '010010110011'
  ];

  /* Discard the fractional part of a number, e.g., INT(3.2) = 3 */
  castInt(double d) {
    return d.toInt();
  }

/* Compute the (integral) Julian day number of day dd/mm/yyyy, i.e., the number 
 * of days between 1/1/4713 BC (Julian calendar) and dd/mm/yyyy. 
 * Formula from http://www.tondering.dk/claus/calendar.html
 */
  num jdFromDate(num dd, num mm, num yy) {
    num a, y, m, jd;
    a = castInt((14 - mm) / 12);
    y = yy + 4800 - a;
    m = mm + 12 * a - 3;
    jd = dd +
        castInt((153 * m + 2) / 5) +
        365 * y +
        castInt(y / 4) -
        castInt(y / 100) +
        castInt(y / 400) -
        32045;
    if (jd < 2299161) {
      jd = dd + castInt((153 * m + 2) / 5) + 365 * y + castInt(y / 4) - 32083;
    }
    return jd;
  }

/* Convert a Julian day number to day/month/year. Parameter jd is an integer */
  List<num> jdToDate(num jd) {
    num a, b, c, d, e, m, day, month, year;
    if (jd > 2299160) {
      // After 5/10/1582, Gregorian calendar
      a = jd + 32044;
      b = castInt((4 * a + 3) / 146097);
      c = a - castInt((b * 146097) / 4);
    } else {
      b = 0;
      c = jd + 32082;
    }
    d = castInt((4 * c + 3) / 1461);
    e = c - castInt((1461 * d) / 4);
    m = castInt((5 * e + 2) / 153);
    day = e - castInt((153 * m + 2) / 5) + 1;
    month = m + 3 - 12 * castInt(m / 10);
    year = b * 100 + d - 4800 + castInt(m / 10);
    return [day, month, year];
  }

/* Compute the time of the k-th new moon after the new moon of 1/1/1900 13:52 UCT 
 * (measured as the number of days since 1/1/4713 BC noon UCT, e.g., 2451545.125 is 1/1/2000 15:00 UTC).
 * Returns a floating number, e.g., 2415079.9758617813 for k=2 or 2414961.935157746 for k=-2
 * Algorithm from: "Astronomical Algorithms" by Jean Meeus, 1998
 */
  num newMoon(num k) {
    num t, t2, t3, dr, jd1, M, mpr, f, c1, deltat, jdNew;
    t = k / 1236.85; // Time in Julian centuries from 1900 January 0.5
    t2 = t * t;
    t3 = t2 * t;
    dr = pi / 180;
    jd1 = 2415020.75933 + 29.53058868 * k + 0.0001178 * t2 - 0.000000155 * t3;
    jd1 = jd1 +
        0.00033 *
            sin((166.56 + 132.87 * t - 0.009173 * t2) * dr); // Mean new moon
    M = 359.2242 +
        29.10535608 * k -
        0.0000333 * t2 -
        0.00000347 * t3; // Sun's mean anomaly
    mpr = 306.0253 +
        385.81691806 * k +
        0.0107306 * t2 +
        0.00001236 * t3; // Moon's mean anomaly
    f = 21.2964 +
        390.67050646 * k -
        0.0016528 * t2 -
        0.00000239 * t3; // Moon's argument of latitude
    c1 = (0.1734 - 0.000393 * t) * sin(M * dr) + 0.0021 * sin(2 * dr * M);
    c1 = c1 - 0.4068 * sin(mpr * dr) + 0.0161 * sin(dr * 2 * mpr);
    c1 = c1 - 0.0004 * sin(dr * 3 * mpr);
    c1 = c1 + 0.0104 * sin(dr * 2 * f) - 0.0051 * sin(dr * (M + mpr));
    c1 = c1 - 0.0074 * sin(dr * (M - mpr)) + 0.0004 * sin(dr * (2 * f + M));
    c1 = c1 - 0.0004 * sin(dr * (2 * f - M)) - 0.0006 * sin(dr * (2 * f + mpr));
    c1 = c1 +
        0.0010 * sin(dr * (2 * f - mpr)) +
        0.0005 * sin(dr * (2 * mpr + M));
    if (t < -11) {
      deltat = 0.001 +
          0.000839 * t +
          0.0002261 * t2 -
          0.00000845 * t3 -
          0.000000081 * t * t3;
    } else {
      deltat = -0.000278 + 0.000265 * t + 0.000262 * t2;
    }
    jdNew = jd1 + c1 - deltat;
    return jdNew;
  }

/* Compute the longitude of the sun at any time. 
 * Parameter: floating number jdn, the number of days since 1/1/4713 BC noon
 * Algorithm from: "Astronomical Algorithms" by Jean Meeus, 1998
 */
  num sunLongitude(num jdn) {
    num T, t2, dr, M, l0, dL, L;
    T = (jdn - 2451545.0) /
        36525; // Time in Julian centuries from 2000-01-01 12:00:00 GMT
    t2 = T * T;
    dr = pi / 180; // degree to radian
    M = 357.52910 +
        35999.05030 * T -
        0.0001559 * t2 -
        0.00000048 * T * t2; // mean anomaly, degree
    l0 = 280.46645 + 36000.76983 * T + 0.0003032 * t2; // mean longitude, degree
    dL = (1.914600 - 0.004817 * T - 0.000014 * t2) * sin(dr * M);
    dL = dL +
        (0.019993 - 0.000101 * T) * sin(dr * 2 * M) +
        0.000290 * sin(dr * 3 * M);
    L = l0 + dL; // true longitude, degree
    L = L * dr;
    L = L - pi * 2 * (castInt(L / (pi * 2))); // Normalize to (0, 2*PI)
    return L;
  }

/* Compute sun position at midnight of the day with the given Julian day number. 
 * The time zone if the time difference between local time and UTC: 7.0 for UTC+7:00.
 * The  returns a number between 0 and 11. 
 * From the day after March equinox and the 1st major term after March equinox, 0 is returned. 
 * After that, return 1, 2, 3 ... 
 */
  getSunLongitude(dayNumber, timeZone) {
    return castInt(sunLongitude(dayNumber - 0.5 - timeZone / 24) / pi * 6);
  }

/* Compute the day of the k-th new moon in the given time zone.
 * The time zone if the time difference between local time and UTC: 7.0 for UTC+7:00
 */
  int getNewMoonDay(num k, num timeZone) {
    return castInt(newMoon(k) + 0.5 + timeZone / 24);
  }

/* Find the day that starts the luner month 11 of the given year for the given time zone */
  num getLunarMonth11(num yy, num timeZone) {
    num k, off, nm, sunLong;
    //off = jdFromDate(31, 12, yy) - 2415021.076998695;
    off = jdFromDate(31, 12, yy) - 2415021;
    k = castInt(off / 29.530588853);
    nm = getNewMoonDay(k, timeZone);
    sunLong = getSunLongitude(nm, timeZone); // sun longitude at local midnight
    if (sunLong >= 9) {
      nm = getNewMoonDay(k - 1, timeZone);
    }
    return nm;
  }

/* Find the index of the leap month after the month starting on the day a11. */
  num getLeapMonthOffset(num a11, num timeZone) {
    num k, last, arc, i;
    k = castInt((a11 - 2415021.076998695) / 29.530588853 + 0.5);
    last = 0;
    i = 1; // We start with the month following lunar month 11
    arc = getSunLongitude(getNewMoonDay(k + i, timeZone), timeZone);
    do {
      last = arc;
      i++;
      arc = getSunLongitude(getNewMoonDay(k + i, timeZone), timeZone);
    } while (arc != last && i < 14);
    return i - 1;
  }

/* Comvert solar date dd/mm/yyyy to the corresponding lunar date */
  List<num> convertSolar2Lunar(num dd, num mm, num yy, num timeZone) {
    num k,
        dayNumber,
        monthStart,
        a11,
        b11,
        lunarDay,
        lunarMonth,
        lunarYear,
        lunarLeap;
    dayNumber = jdFromDate(dd, mm, yy);
    k = castInt((dayNumber - 2415021.076998695) / 29.530588853);
    monthStart = getNewMoonDay(k + 1, timeZone);
    if (monthStart > dayNumber) {
      monthStart = getNewMoonDay(k, timeZone);
    }
    //alert(dayNumber+" -> "+monthStart);
    a11 = getLunarMonth11(yy, timeZone);
    b11 = a11;
    if (a11 >= monthStart) {
      lunarYear = yy;
      a11 = getLunarMonth11(yy - 1, timeZone);
    } else {
      lunarYear = yy + 1;
      b11 = getLunarMonth11(yy + 1, timeZone);
    }
    lunarDay = dayNumber - monthStart + 1;
    var diff = castInt((monthStart - a11) / 29);
    lunarLeap = 0;
    lunarMonth = diff + 11;
    if (b11 - a11 > 365) {
      var leapMonthDiff = getLeapMonthOffset(a11, timeZone);
      if (diff >= leapMonthDiff) {
        lunarMonth = diff + 10;
        if (diff == leapMonthDiff) {
          lunarLeap = 1;
        }
      }
    }
    if (lunarMonth > 12) {
      lunarMonth = lunarMonth - 12;
    }
    if (lunarMonth >= 11 && diff < 4) {
      lunarYear -= 1;
    }
    return [lunarDay, lunarMonth, lunarYear, lunarLeap];
  }

/* Convert a lunar date to the corresponding solar date */
  List<num> convertLunar2Solar(
    num lunarDay,
    num lunarMonth,
    num lunarYear,
    num lunarLeap,
    num timeZone,
  ) {
    num k, a11, b11, off, leapOff, leapMonth, monthStart;
    if (lunarMonth < 11) {
      a11 = getLunarMonth11(lunarYear - 1, timeZone);
      b11 = getLunarMonth11(lunarYear, timeZone);
    } else {
      a11 = getLunarMonth11(lunarYear, timeZone);
      b11 = getLunarMonth11(lunarYear + 1, timeZone);
    }
    k = castInt(0.5 + (a11 - 2415021.076998695) / 29.530588853);
    off = lunarMonth - 11;
    if (off < 0) {
      off += 12;
    }
    if (b11 - a11 > 365) {
      leapOff = getLeapMonthOffset(a11, timeZone);
      leapMonth = leapOff - 2;
      if (leapMonth < 0) {
        leapMonth += 12;
      }
      if (lunarLeap != 0 && lunarMonth != leapMonth) {
        return [0, 0, 0];
      } else if (lunarLeap != 0 || off >= leapOff) {
        off += 1;
      }
    }
    monthStart = getNewMoonDay(k + off, timeZone);
    return jdToDate(monthStart + lunarDay - 1);
  }

  String getCanChiYear(int year) {
    var can = canList[year % 10];
    var chi = chiList[year % 12];
    return '$can $chi';
  }

  String getCanChiMonth(int month, int year) {
    var chi = chiForMonthList[month - 1];
    var indexCan = 0;
    var can = canList[year % 10];

    if (can == "Giáp" || can == "Kỉ") {
      indexCan = 6;
    }
    if (can == "Ất" || can == "Canh") {
      indexCan = 8;
    }
    if (can == "Bính" || can == "Tân") {
      indexCan = 0;
    }
    if (can == "Đinh" || can == "Nhâm") {
      indexCan = 2;
    }
    if (can == "Mậu" || can == "Quý") {
      indexCan = 4;
    }
    return '${canList[(indexCan + month - 1) % 10]} $chi';
  }

  String getYearCanChi(int year) {
    return "${can[(year + 6) % 10]} ${chi[(year + 8) % 12]}";
  }

  String getCanHour(jdn) {
    return can[(jdn - 1) * 2 % 10];
  }

  String getCanDay(jdn) {
    String dayName;
    dayName = "${can[(jdn + 9) % 10]} ${chi[(jdn + 1) % 12]}";
    return dayName;
  }

  num jdn(dd, mm, yy) {
    var a = castInt((14 - mm) / 12);
    var y = yy + 4800 - a;
    var m = mm + 12 * a - 3;
    var jd = dd +
        castInt((153 * m + 2) / 5) +
        365 * y +
        castInt(y / 4) -
        castInt(y / 100) +
        castInt(y / 400) -
        32045;
    return jd;
  }

  String getGioHoangDao(jd) {
    var chiOfDay = (jd + 1) % 12;
    var gioHD = gioHoangDao[chiOfDay %
        6]; // same values for Ty' (1) and Ngo. (6), for Suu and Mui etc.
    var ret = "";
    var count = 0;
    for (var i = 0; i < 12; i++) {
      if (gioHD.substring(i, i + 1) == '1') {
        ret += chi[i];
        ret += ' (${{(i * 2 + 23) % 24}}-${{(i * 2 + 1) % 24}})';
        if (count++ < 5) ret += ', ';
        if (count == 3) ret += '\n';
      }
    }
    return ret;
  }

  String getTietKhi(jd) {
    return tietKhi[getSunLongitude(jd + 1, 7.0)];
  }

  String getBeginHour(jdn) {
    return '${can[(jdn - 1) * 2 % 10]} ${chi[0]}';
  }
}
