import '../../constants/app_text.dart';

extension DateTimeExtension on DateTime {
  String get weekdayName {
    switch (weekday) {
      case 7:
        return AppText.sunday;
      case 6:
        return AppText.saturday;
      case 5:
        return AppText.friday;
      case 4:
        return AppText.thursday;
      case 3:
        return AppText.wednesday;
      case 2:
        return AppText.tuesday;
      case 1:
        return AppText.monday;
      default:
        return '';
    }
  }

  bool get isSunday => weekday == 7;
}
