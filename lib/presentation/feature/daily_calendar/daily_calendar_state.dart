import 'package:freezed_annotation/freezed_annotation.dart';

import '../../../data/models/perpetual_calendar.dart';
import '../../utils/enums/swipe_direction.dart';

part 'daily_calendar_state.freezed.dart';

@freezed
class DailyCalendarState with _$DailyCalendarState {
  const factory DailyCalendarState({
    required PerpetualCalendar perpetualCalendar,
    required SwipeDirection swipeDirection,
    required String lastBackgroundImage,
  }) = _DailyCalendarState;

  factory DailyCalendarState.init() {
    final now = DateTime.now();
    final perpetualCalendar = PerpetualCalendar(now);

    return DailyCalendarState(
      perpetualCalendar: perpetualCalendar,
      swipeDirection: SwipeDirection.leftToRight,
      lastBackgroundImage: perpetualCalendar.dailyQuotes.backgroundImage,
    );
  }
}
