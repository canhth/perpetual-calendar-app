import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';

import '../../../data/models/perpetual_calendar.dart';
import '../../constants/dimensions.dart';
import '../../utils/enums/swipe_direction.dart';
import '../../widgets/date_selector_button.dart';
import 'daily_calendar_cubit.dart';
import 'daily_calendar_state.dart';
import 'widgets/daily_background_widget.dart';
import 'widgets/daily_quotes_widget.dart';
import 'widgets/lunar_date_info_widget.dart';
import 'widgets/solar_date_info_widget.dart';

class DailyCalendarPage extends StatelessWidget {
  const DailyCalendarPage({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => DailyCalendarCubit(DailyCalendarState.init()),
      child: BlocBuilder<DailyCalendarCubit, DailyCalendarState>(
        builder: (context, state) {
          final perpetualCalendar = state.perpetualCalendar;
          final selectedDate = perpetualCalendar.solarDate;

          final titleAppbar =
              'Tháng ${DateFormat('MM - yyyy').format(selectedDate)}';

          const foregroundColor = Colors.white;

          return Scaffold(
            appBar: AppBar(
              foregroundColor: foregroundColor,
              backgroundColor: Colors.transparent,
              leading: const Icon(Icons.cloud_outlined),
              centerTitle: true,
              title: DateSelectorButton(
                selectedDate: selectedDate,
                onTodayBtnTap: () {
                  final now = DateTime.now();
                  context
                      .read<DailyCalendarCubit>()
                      .updatePerpetualCalendar(PerpetualCalendar(now));
                },
                onSelectBtnTap: (DateTime newDate) {
                  context
                      .read<DailyCalendarCubit>()
                      .updatePerpetualCalendar(PerpetualCalendar(newDate));
                },
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Text(
                      titleAppbar,
                      style: const TextStyle(fontSize: 16),
                    ),
                    const Icon(Icons.expand_more),
                  ],
                ),
              ),
              actions: const [
                Icon(Icons.calendar_month),
                SizedBox(width: Dimensions.spacing16)
              ],
            ),
            extendBodyBehindAppBar: true,
            body: const _BodyView(),
          );
        },
      ),
    );
  }
}

class _BodyView extends StatelessWidget {
  const _BodyView();

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onHorizontalDragEnd: (DragEndDetails details) {
        if (details.primaryVelocity == null) {
          return;
        }

        // Swiping from left to right direction.
        if (details.primaryVelocity! > 1000) {
          context
              .read<DailyCalendarCubit>()
              .updateSwipeDirection(SwipeDirection.leftToRight);
          context.read<DailyCalendarCubit>().dayDecrease();
        }

        // Swiping from right to left direction.
        if (details.primaryVelocity! < -1000) {
          context
              .read<DailyCalendarCubit>()
              .updateSwipeDirection(SwipeDirection.rightToLeft);
          context.read<DailyCalendarCubit>().dayIncrease();
        }
      },
      onVerticalDragEnd: (details) {
        if (details.primaryVelocity == null) {
          return;
        }

        // Swiping from top to bottom direction.
        if (details.primaryVelocity! > 1000) {
          context
              .read<DailyCalendarCubit>()
              .updateSwipeDirection(SwipeDirection.topToBottom);
          context.read<DailyCalendarCubit>().monthDecrease();
        }

        // Swiping from bottom to top direction.
        if (details.primaryVelocity! < -1000) {
          context
              .read<DailyCalendarCubit>()
              .updateSwipeDirection(SwipeDirection.bottomToTop);
          context.read<DailyCalendarCubit>().monthIncrease();
        }
      },
      child: Stack(
        children: [
          const DailyBackgroundWidget(),
          SizedBox(
            width: double.infinity,
            child: Column(
              children: const [
                SolarDateInfoWidget(),
                Spacer(),
                DailyQuotesWidget(),
                LunarDateInfoWidget(),
                SizedBox(
                  height: kBottomNavigationBarHeight + Dimensions.spacing8,
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
