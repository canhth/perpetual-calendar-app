import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../constants/dimensions.dart';
import '../../../utils/extensions/date_time_extension.dart';
import '../daily_calendar_cubit.dart';
import '../daily_calendar_state.dart';

class SolarDateInfoWidget extends StatelessWidget {
  const SolarDateInfoWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<DailyCalendarCubit, DailyCalendarState>(
      builder: (context, state) {
        const textColor = Colors.white;

        final solarDate = state.perpetualCalendar.solarDate;

        return Padding(
          padding: const EdgeInsets.only(
            top: 100,
            left: Dimensions.spacing16,
            right: Dimensions.spacing16,
            bottom: 0,
          ),
          child: Column(
            children: [
              Stack(
                children: [
                  Text(
                    solarDate.day.toString(),
                    style: TextStyle(
                      height: 1,
                      fontSize: 140,
                      fontWeight: FontWeight.w500,
                      foreground: Paint()
                        ..style = PaintingStyle.stroke
                        ..strokeWidth = 8
                        ..color = Colors.blue.shade800,
                    ),
                  ),
                  Text(
                    solarDate.day.toString(),
                    style: const TextStyle(
                      height: 1,
                      fontSize: 140,
                      fontWeight: FontWeight.w500,
                      color: textColor,
                    ),
                  ),
                ],
              ),
              Text(
                solarDate.weekdayName.toUpperCase(),
                style: const TextStyle(
                  height: 1,
                  fontSize: 32,
                  fontWeight: FontWeight.w500,
                  color: textColor,
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
