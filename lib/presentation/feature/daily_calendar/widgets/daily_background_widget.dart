import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../utils/enums/swipe_direction.dart';
import '../daily_calendar_cubit.dart';
import '../daily_calendar_state.dart';

class DailyBackgroundWidget extends StatefulWidget {
  const DailyBackgroundWidget({
    super.key,
  });

  @override
  State<DailyBackgroundWidget> createState() => _DailyBackgroundWidgetState();
}

class _DailyBackgroundWidgetState extends State<DailyBackgroundWidget>
    with TickerProviderStateMixin {
  late AnimationController _controller;
  late Animation<Offset> _animation;
  @override
  void initState() {
    super.initState();

    _controller = AnimationController(
      duration: const Duration(milliseconds: 300),
      vsync: this,
    );

    _animation = Tween<Offset>(
      begin: const Offset(0.0, 0.0),
      end: const Offset(0.0, 0.0),
    ).animate(
      CurvedAnimation(
        parent: _controller,
        curve: Curves.fastOutSlowIn,
      ),
    );
  }

  @override
  void dispose() {
    _controller.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<DailyCalendarCubit, DailyCalendarState>(
      listenWhen: (previous, current) {
        return previous.perpetualCalendar.dailyQuotes.backgroundImage !=
            current.perpetualCalendar.dailyQuotes.backgroundImage;
      },
      listener: (context, state) {
        _animation = Tween<Offset>(
          begin: state.swipeDirection.toBeginOffset(),
          end: const Offset(0.0, 0.0),
        ).animate(
          CurvedAnimation(
            parent: _controller,
            curve: Curves.easeInCubic,
          ),
        );
        _controller
          ..reset()
          ..forward().then((_) {
            context.read<DailyCalendarCubit>().updateLastBackgroundImage(
                  state.perpetualCalendar.dailyQuotes.backgroundImage,
                );
          });
      },
      buildWhen: (previous, current) {
        return previous.perpetualCalendar.dailyQuotes.backgroundImage !=
            current.perpetualCalendar.dailyQuotes.backgroundImage;
      },
      builder: (context, state) {
        final lastImgPath = state.lastBackgroundImage;
        final imgPath = state.perpetualCalendar.dailyQuotes.backgroundImage;

        return SizedBox(
          height: double.infinity,
          width: double.infinity,
          child: Stack(
            children: [
              // Temporary background before swiping
              Positioned.fill(
                child: Image.asset(
                  lastImgPath,
                  fit: BoxFit.cover,
                ),
              ),
              // Background after swiping
              Positioned.fill(
                child: SlideTransition(
                  position: _animation,
                  child: Image.asset(
                    imgPath,
                    fit: BoxFit.cover,
                    key: ValueKey<String>(imgPath),
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
