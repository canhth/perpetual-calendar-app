import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../data/models/perpetual_calendar.dart';
import '../../../constants/dimensions.dart';
import '../daily_calendar_cubit.dart';
import '../daily_calendar_state.dart';
import 'current_hour_text.dart';

class LunarDateInfoWidget extends StatelessWidget {
  const LunarDateInfoWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<DailyCalendarCubit, DailyCalendarState>(
      builder: (context, state) {
        final PerpetualCalendar perpetualCalendar = state.perpetualCalendar;

        final String lunarHourByText = perpetualCalendar.lunarHourByText;
        final String lunarDayByText = perpetualCalendar.lunarDayByText;
        final String lunarMonthByText = perpetualCalendar.lunarMonthByText;
        final String lunarYearByText = perpetualCalendar.lunarYearByText;

        final DateTime lunarDate = perpetualCalendar.lunarDate;

        const textColor = Colors.white;

        const textStyle14 = TextStyle(
          fontSize: 14,
          color: textColor,
        );

        return Container(
          height: 120,
          width: double.infinity,
          padding: const EdgeInsets.symmetric(
            horizontal: Dimensions.spacing16,
            vertical: Dimensions.spacing8,
          ),
          decoration: const BoxDecoration(
            color: Colors.white30,
          ),
          child: Row(
            children: [
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const CurrentHourText(),
                    Text(
                      'Giờ $lunarHourByText',
                      style: textStyle14,
                    )
                  ],
                ),
              ),
              const VerticalDivider(
                width: 0.5,
                color: Colors.white38,
              ),
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      lunarDate.day.toString(),
                      style: const TextStyle(
                        height: 1,
                        fontSize: 46,
                        color: textColor,
                      ),
                    ),
                    Text(
                      'Tháng ${lunarDate.month}',
                      style: textStyle14,
                    ),
                  ],
                ),
              ),
              const VerticalDivider(
                width: 0.5,
                color: Colors.white38,
              ),
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      'Ngày $lunarDayByText',
                      style: textStyle14,
                    ),
                    Text(
                      'Tháng $lunarMonthByText',
                      style: textStyle14,
                    ),
                    Text(
                      'Năm $lunarYearByText',
                      style: textStyle14,
                    ),
                  ],
                ),
              )
            ],
          ),
        );
      },
    );
  }
}
