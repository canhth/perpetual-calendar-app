import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../constants/dimensions.dart';
import '../daily_calendar_cubit.dart';
import '../daily_calendar_state.dart';

class DailyQuotesWidget extends StatelessWidget {
  const DailyQuotesWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<DailyCalendarCubit, DailyCalendarState>(
      builder: (context, state) {
        final dailyQuotes = state.perpetualCalendar.dailyQuotes;

        const textStyle = TextStyle(
          fontSize: 14,
          color: Colors.white,
        );

        return Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: Dimensions.spacing16,
            vertical: Dimensions.spacing8,
          ),
          child: Column(
            children: [
              Text(
                dailyQuotes.content,
                style: textStyle,
                textAlign: TextAlign.center,
              ),
              if (dailyQuotes.author != null)
                Align(
                  alignment: Alignment.centerRight,
                  child: Text(
                    dailyQuotes.author.toString(),
                    style: textStyle,
                    textAlign: TextAlign.right,
                  ),
                ),
            ],
          ),
        );
      },
    );
  }
}
