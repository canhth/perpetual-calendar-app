import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';

import '../daily_calendar_cubit.dart';
import '../daily_calendar_state.dart';

class CurrentHourText extends StatefulWidget {
  const CurrentHourText({
    super.key,
  });

  @override
  State<CurrentHourText> createState() => _CurrentHourTextState();
}

class _CurrentHourTextState extends State<CurrentHourText> {
  late Timer _timer;

  DateTime now = DateTime.now();

  @override
  void initState() {
    super.initState();
    // Update real-time
    _timer = Timer.periodic(const Duration(seconds: 2), (timer) {
      setState(() {
        now = DateTime.now();
      });
    });
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<DailyCalendarCubit, DailyCalendarState>(
      builder: (context, state) {
        const textColor = Colors.white;

        return Text(
          DateFormat.Hm().format(now),
          style: const TextStyle(
            fontSize: 24,
            color: textColor,
          ),
        );
      },
    );
  }
}
