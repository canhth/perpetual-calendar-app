import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:jiffy/jiffy.dart';

import '../../../data/models/perpetual_calendar.dart';
import '../../utils/enums/swipe_direction.dart';
import 'daily_calendar_state.dart';

class DailyCalendarCubit extends Cubit<DailyCalendarState> {
  DailyCalendarCubit(super.initialState);

  DateTime get solarDate => state.perpetualCalendar.solarDate;

  void updatePerpetualCalendar(PerpetualCalendar value) => emit(
        state.copyWith(perpetualCalendar: value),
      );

  void updateSwipeDirection(SwipeDirection value) => emit(
        state.copyWith(swipeDirection: value),
      );

  void dayIncrease() => emit(
        state.copyWith(
          perpetualCalendar: PerpetualCalendar(
            solarDate.add(const Duration(days: 1)),
          ),
        ),
      );

  void dayDecrease() => emit(
        state.copyWith(
          perpetualCalendar: PerpetualCalendar(
            solarDate.subtract(const Duration(days: 1)),
          ),
        ),
      );

  void monthIncrease() => emit(
        state.copyWith(
          perpetualCalendar: PerpetualCalendar(
            Jiffy(solarDate).add(months: 1).dateTime,
          ),
        ),
      );

  void monthDecrease() => emit(
        state.copyWith(
          perpetualCalendar: PerpetualCalendar(
            Jiffy(solarDate).subtract(months: 1).dateTime,
          ),
        ),
      );

  void updateLastBackgroundImage(String value) => emit(
        state.copyWith(lastBackgroundImage: value),
      );
}
