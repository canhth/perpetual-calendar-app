import 'package:freezed_annotation/freezed_annotation.dart';

import '../../../data/models/perpetual_calendar.dart';

part 'date_calendar_detail_state.freezed.dart';

@freezed
class DateCalendarDetailState with _$DateCalendarDetailState {
  const factory DateCalendarDetailState({
    required PerpetualCalendar perpetualCalendar,
  }) = _DateCalendarDetailState;
}
