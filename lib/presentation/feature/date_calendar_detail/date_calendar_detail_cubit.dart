import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:table_calendar/table_calendar.dart';

import '../../../data/models/perpetual_calendar.dart';
import 'date_calendar_detail_state.dart';

class DateCalendarDetailCubit extends Cubit<DateCalendarDetailState> {
  DateCalendarDetailCubit(super.initialState);

  DateTime get solarDate => state.perpetualCalendar.solarDate;

  void updatePerpetualCalendar(PerpetualCalendar value) => emit(
        state.copyWith(perpetualCalendar: value),
      );

  void solarDayIncrease() => emit(
        state.copyWith(
          perpetualCalendar: PerpetualCalendar(
            solarDate.add(const Duration(days: 1)),
          ),
        ),
      );

  void solarDayDecrease() => emit(
        state.copyWith(
          perpetualCalendar: PerpetualCalendar(
            solarDate.subtract(const Duration(days: 1)),
          ),
        ),
      );

  bool get showTodayBtn => !isSameDay(solarDate, DateTime.now());
}
