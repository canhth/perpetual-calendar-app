import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';

import '../../../data/models/perpetual_calendar.dart';
import '../../constants/app_text.dart';
import '../../constants/dimensions.dart';
import '../../widgets/gradient_background.dart';
import 'date_calendar_detail_cubit.dart';
import 'date_calendar_detail_state.dart';
import 'widgets/departure_direction_card.dart';
import 'widgets/good_stars_bad_stars_card.dart';
import 'widgets/lunar_card.dart';
import 'widgets/things_not_to_do_card.dart';
import 'widgets/things_to_do_card.dart';
import 'widgets/zodiac_hour_card.dart';

class DateCalendarDetailPage extends StatelessWidget {
  const DateCalendarDetailPage({
    super.key,
    required this.selectedDay,
  });

  final DateTime selectedDay;

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => DateCalendarDetailCubit(
        DateCalendarDetailState(
          perpetualCalendar: PerpetualCalendar(selectedDay),
        ),
      ),
      child: BlocBuilder<DateCalendarDetailCubit, DateCalendarDetailState>(
        builder: (context, state) {
          final solarDay = state.perpetualCalendar.solarDate;

          final titleAppBar = DateFormat.yMEd('vi').format(solarDay);

          final showTodayBtn =
              context.watch<DateCalendarDetailCubit>().showTodayBtn;

          return Stack(
            children: [
              const GradientBackground(),
              Scaffold(
                appBar: AppBar(
                  centerTitle: true,
                  leading: IconButton(
                    iconSize: 32,
                    icon: const Icon(Icons.arrow_circle_left_outlined),
                    onPressed: () {
                      context
                          .read<DateCalendarDetailCubit>()
                          .solarDayDecrease();
                    },
                  ),
                  title: Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      if (showTodayBtn) const _TodayButton(),
                      Text(
                        titleAppBar,
                        style: const TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                    ],
                  ),
                  actions: [
                    IconButton(
                      iconSize: 32,
                      icon: const Icon(Icons.arrow_circle_right_outlined),
                      onPressed: () {
                        context
                            .read<DateCalendarDetailCubit>()
                            .solarDayIncrease();
                      },
                    ),
                  ],
                ),
                body: const _BodyView(),
              ),
            ],
          );
        },
      ),
    );
  }
}

class _BodyView extends StatelessWidget {
  const _BodyView();

  @override
  Widget build(BuildContext context) {
    const items = [
      LunarCard(),
      ZodiacHourCard(),
      DepartureDirectionCard(),
      GoodStarsBadStarsCard(),
      ThingsToDoCard(),
      ThingsNotToDoCard(),
    ];
    return ListView.separated(
      padding: const EdgeInsets.all(Dimensions.spacing16),
      shrinkWrap: true,
      itemCount: items.length,
      itemBuilder: (context, index) => items[index],
      separatorBuilder: (context, index) {
        return const SizedBox(height: Dimensions.spacing16);
      },
    );
  }
}

class _TodayButton extends StatelessWidget {
  const _TodayButton();

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<DateCalendarDetailCubit, DateCalendarDetailState>(
      builder: (context, state) {
        return TextButton(
          onPressed: () {
            final now = DateTime.now();
            context
                .read<DateCalendarDetailCubit>()
                .updatePerpetualCalendar(PerpetualCalendar(now));
          },
          child: Text(
            AppText.todayBtn,
            style: TextStyle(color: Colors.blue.shade900),
          ),
        );
      },
    );
  }
}
