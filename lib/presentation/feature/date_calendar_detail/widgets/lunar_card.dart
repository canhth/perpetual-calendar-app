import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../data/models/perpetual_calendar.dart';
import '../../../constants/app_text.dart';
import '../date_calendar_detail_cubit.dart';
import '../date_calendar_detail_state.dart';
import 'content_card.dart';

class LunarCard extends StatelessWidget {
  const LunarCard({super.key});

  TextStyle get textStyle15 => TextStyle(
        color: Colors.blue.shade900,
        fontSize: 15,
      );

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<DateCalendarDetailCubit, DateCalendarDetailState>(
      builder: (context, state) {
        final PerpetualCalendar perpetualCalendar = state.perpetualCalendar;

        final DateTime lunarDate = state.perpetualCalendar.lunarDate;

        return ContentCard(
          title: AppText.lunarCalendar,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Column(
                children: [
                  Text(
                    AppText.day.toUpperCase(),
                    style: textStyle15,
                  ),
                  Text(
                    lunarDate.day.toString(),
                    style: textStyle15,
                  ),
                  Text(
                    perpetualCalendar.lunarDayByText,
                    style: textStyle15,
                  ),
                ],
              ),
              Column(
                children: [
                  Text(
                    AppText.month.toUpperCase(),
                    style: textStyle15,
                  ),
                  Text(
                    lunarDate.month.toString(),
                    style: textStyle15,
                  ),
                  Text(
                    perpetualCalendar.lunarMonthByText,
                    style: textStyle15,
                  ),
                ],
              ),
              Column(
                children: [
                  Text(
                    AppText.year.toUpperCase(),
                    style: textStyle15,
                  ),
                  Text(
                    lunarDate.year.toString(),
                    style: textStyle15,
                  ),
                  Text(
                    perpetualCalendar.lunarYearByText,
                    style: textStyle15,
                  ),
                ],
              )
            ],
          ),
        );
      },
    );
  }
}
