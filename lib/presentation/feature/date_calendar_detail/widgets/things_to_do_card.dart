import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../constants/app_text.dart';
import '../date_calendar_detail_cubit.dart';
import '../date_calendar_detail_state.dart';
import 'content_card.dart';

class ThingsToDoCard extends StatelessWidget {
  const ThingsToDoCard({super.key});

  TextStyle get textStyle16 => TextStyle(
        color: Colors.blue.shade900,
        fontSize: 16,
      );

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<DateCalendarDetailCubit, DateCalendarDetailState>(
      builder: (context, state) {
        final String thingsToDo = state.perpetualCalendar.thingsToDo;

        return ContentCard(
          title: AppText.thingsToDo,
          child: Text(
            thingsToDo,
            style: textStyle16,
          ),
        );
      },
    );
  }
}
