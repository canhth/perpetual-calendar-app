import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../data/models/zodiac_hour.dart';
import '../../../constants/app_text.dart';
import '../date_calendar_detail_cubit.dart';
import '../date_calendar_detail_state.dart';
import 'content_card.dart';

class ZodiacHourCard extends StatelessWidget {
  const ZodiacHourCard({super.key});

  TextStyle get textStyle14 => TextStyle(
        color: Colors.blue.shade900,
        fontSize: 14,
      );

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<DateCalendarDetailCubit, DateCalendarDetailState>(
      builder: (context, state) {
        final List<ZodiacHour> zodiacHourList =
            state.perpetualCalendar.zodiacHourList;

        return ContentCard(
          title: AppText.zodiacHour,
          child: GridView.count(
            shrinkWrap: true,
            crossAxisCount: 3,
            children: zodiacHourList
                .map(
                  (zodiacHour) => Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Image.asset(
                        zodiacHour.imgPath,
                        color: Colors.blue.shade900,
                        width: 50,
                        height: 50,
                      ),
                      Text(
                        zodiacHour.zodiacHourText,
                        style: textStyle14,
                      ),
                      Text(
                        '(${zodiacHour.hourStarts}h-${zodiacHour.hourEnds}h)',
                        style: textStyle14,
                      ),
                    ],
                  ),
                )
                .toList(),
          ),
        );
      },
    );
  }
}
