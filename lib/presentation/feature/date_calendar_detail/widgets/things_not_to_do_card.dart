import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../constants/app_text.dart';
import '../date_calendar_detail_cubit.dart';
import '../date_calendar_detail_state.dart';
import 'content_card.dart';

class ThingsNotToDoCard extends StatelessWidget {
  const ThingsNotToDoCard({super.key});

  TextStyle get textStyle16 => TextStyle(
        color: Colors.blue.shade900,
        fontSize: 16,
      );

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<DateCalendarDetailCubit, DateCalendarDetailState>(
      builder: (context, state) {
        final String thingsNotToDo = state.perpetualCalendar.thingsNotToDo;

        return ContentCard(
          title: AppText.thingsNotToDo,
          child: Text(
            thingsNotToDo,
            style: textStyle16,
          ),
        );
      },
    );
  }
}
