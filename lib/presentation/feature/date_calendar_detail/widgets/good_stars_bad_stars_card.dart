import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../constants/app_text.dart';
import '../../../constants/dimensions.dart';
import '../date_calendar_detail_cubit.dart';
import '../date_calendar_detail_state.dart';
import 'content_card.dart';

class GoodStarsBadStarsCard extends StatelessWidget {
  const GoodStarsBadStarsCard({super.key});

  TextStyle get textStyle16 => TextStyle(
        color: Colors.blue.shade900,
        fontSize: 16,
      );

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<DateCalendarDetailCubit, DateCalendarDetailState>(
      builder: (context, state) {
        final List<String> goodStarList = state.perpetualCalendar.goodStarList;
        final List<String> badStarList = state.perpetualCalendar.badStarList;

        return ContentCard(
          title: AppText.goodStarsBadStars,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                '${AppText.goodStars}:',
                style: textStyle16,
              ),
              ...goodStarList
                  .map((text) => Padding(
                        padding: const EdgeInsets.symmetric(
                          vertical: Dimensions.spacing4,
                        ),
                        child: Text(text, style: textStyle16),
                      ))
                  .toList(),
              const SizedBox(height: Dimensions.spacing16),
              Text(
                '${AppText.badStars}:',
                style: textStyle16,
              ),
              ...badStarList
                  .map((text) => Padding(
                        padding: const EdgeInsets.symmetric(
                          vertical: Dimensions.spacing4,
                        ),
                        child: Text(text, style: textStyle16),
                      ))
                  .toList(),
            ],
          ),
        );
      },
    );
  }
}
