import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../constants/app_text.dart';
import '../../../constants/dimensions.dart';
import '../date_calendar_detail_cubit.dart';
import '../date_calendar_detail_state.dart';
import 'content_card.dart';

class DepartureDirectionCard extends StatelessWidget {
  const DepartureDirectionCard({super.key});

  TextStyle get textStyle16 => TextStyle(
        color: Colors.blue.shade900,
        fontSize: 16,
      );

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<DateCalendarDetailCubit, DateCalendarDetailState>(
      builder: (context, state) {
        final List<String> departureDirectionList =
            state.perpetualCalendar.departureDirectionList;

        return ContentCard(
          title: AppText.departureDirection,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: departureDirectionList
                .map(
                  (text) => Padding(
                    padding: const EdgeInsets.all(Dimensions.spacing8),
                    child: Text(
                      '- $text',
                      style: textStyle16,
                    ),
                  ),
                )
                .toList(),
          ),
        );
      },
    );
  }
}
