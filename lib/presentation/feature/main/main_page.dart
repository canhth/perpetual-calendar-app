import 'package:flutter/material.dart';

import '../../widgets/gradient_background.dart';
import '../daily_calendar/daily_calendar_page.dart';
import '../monthly_calendar/monthly_calendar_page.dart';
import 'main_tab.dart';

class MainPage extends StatefulWidget {
  const MainPage({super.key});

  @override
  State<MainPage> createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  List<Widget> pages = [
    const DailyCalendarPage(),
    const MonthlyCalendarPage(),
    Scaffold(
      body: Center(child: Text(MainTab.dailyCalendar.title)),
    ),
    Scaffold(
      body: Center(child: Text(MainTab.expand.title)),
    ),
  ];

  MainTab _tab = MainTab.dailyCalendar;

  final List<BottomNavigationBarItem> bottomNavigationBarItemList = [
    BottomNavigationBarItem(
      icon: const Icon(Icons.calendar_today),
      label: MainTab.dailyCalendar.title,
    ),
    BottomNavigationBarItem(
      icon: const Icon(Icons.calendar_month),
      label: MainTab.monthlyCalendar.title,
    ),
    BottomNavigationBarItem(
      icon: const Icon(Icons.history),
      label: MainTab.changeDate.title,
    ),
    BottomNavigationBarItem(
      icon: const Icon(Icons.more_horiz),
      label: MainTab.expand.title,
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          const GradientBackground(),
          pages[_tab.index],
        ],
      ),
      extendBody: true,
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: Colors.white30,
        elevation: 0,
        type: BottomNavigationBarType.fixed,
        selectedItemColor: Colors.white,
        unselectedItemColor: Colors.black,
        items: bottomNavigationBarItemList,
        currentIndex: _tab.index,
        onTap: (value) {
          setState(() {
            _tab = MainTab.values[value];
          });
        },
      ),
    );
  }
}
