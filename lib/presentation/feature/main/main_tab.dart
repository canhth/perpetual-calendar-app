import '../../constants/app_text.dart';

enum MainTab {
  dailyCalendar,
  monthlyCalendar,
  changeDate,
  expand,
}

extension MainTabName on MainTab {
  String get title {
    switch (this) {
      case MainTab.dailyCalendar:
        return AppText.dailyCalendar;
      case MainTab.monthlyCalendar:
        return AppText.monthlyCalendar;
      case MainTab.changeDate:
        return AppText.changeDate;
      case MainTab.expand:
        return AppText.expand;
      default:
        return '';
    }
  }
}
