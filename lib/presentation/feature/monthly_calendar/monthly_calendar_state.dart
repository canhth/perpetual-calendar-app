import 'package:freezed_annotation/freezed_annotation.dart';

part 'monthly_calendar_state.freezed.dart';

@freezed
class MonthlyCalendarState with _$MonthlyCalendarState {
  const factory MonthlyCalendarState({
    // The date on which the calendar is focused is displayed.
    required DateTime focusedDay,
    // date selected by the user.
    required DateTime selectedDay,
  }) = _MonthlyCalendarState;

  factory MonthlyCalendarState.init() {
    final now = DateTime.now();

    return MonthlyCalendarState(
      focusedDay: now,
      selectedDay: now,
    );
  }
}
