import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../../../../data/models/perpetual_calendar.dart';
import '../../../utils/extensions/date_time_extension.dart';

class CellWidget extends StatelessWidget {
  const CellWidget({
    super.key,
    required this.day,
    required this.focusedDay,
    this.isOutsideDays = false,
    required this.backgroundColor,
    required this.foregroundColor,
  });

  final DateTime day;
  final DateTime focusedDay;
  final bool isOutsideDays;

  final Color backgroundColor;
  final Color foregroundColor;

  String lunarDateToText(DateTime date) {
    if (date.day == 1 || date.day == 15) {
      return DateFormat('dd/MM').format(date);
    }
    return date.day.toString();
  }

  Color get textColor {
    if (!isOutsideDays && day.isSunday) {
      return Colors.red.shade600;
    }

    return foregroundColor;
  }

  @override
  Widget build(BuildContext context) {
    final DateTime lunarDate = PerpetualCalendar(day).lunarDate;

    return Container(
      decoration: BoxDecoration(
        color: backgroundColor,
        border: Border.all(
          color: Colors.white70,
          width: 0.5,
        ),
      ),
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Text(
              day.day.toString(),
              style: TextStyle(
                color: textColor,
                fontWeight: FontWeight.w600,
                fontSize: 14,
              ),
            ),
            Text(
              lunarDateToText(lunarDate),
              style: TextStyle(
                color: textColor,
                fontWeight: FontWeight.w400,
                fontSize: 12,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
