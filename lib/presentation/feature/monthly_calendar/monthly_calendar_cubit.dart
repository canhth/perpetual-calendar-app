import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:table_calendar/table_calendar.dart';

import 'monthly_calendar_state.dart';

class MonthlyCalendarCubit extends Cubit<MonthlyCalendarState> {
  MonthlyCalendarCubit(super.initialState);

  void updateFocusedDay(DateTime value) => emit(
        state.copyWith(focusedDay: value),
      );

  void updateSelectedDay(DateTime value) => emit(
        state.copyWith(selectedDay: value),
      );

  void updateFocusedAndSelectedDay(DateTime value) => emit(
        state.copyWith(
          selectedDay: value,
          focusedDay: value,
        ),
      );

  bool get showTodayBtn => !isSameDay(state.focusedDay, DateTime.now());
}
