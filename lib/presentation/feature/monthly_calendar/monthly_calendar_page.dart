import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:table_calendar/table_calendar.dart';

import '../../constants/app_text.dart';
import '../../constants/dimensions.dart';
import '../../utils/extensions/date_time_extension.dart';
import '../../widgets/date_selector_button.dart';
import '../date_calendar_detail/date_calendar_detail_page.dart';
import 'monthly_calendar_cubit.dart';
import 'monthly_calendar_state.dart';
import 'widgets/cell_widget.dart';

class MonthlyCalendarPage extends StatelessWidget {
  const MonthlyCalendarPage({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => MonthlyCalendarCubit(MonthlyCalendarState.init()),
      child: BlocBuilder<MonthlyCalendarCubit, MonthlyCalendarState>(
        builder: (context, state) {
          final focusedDay = state.focusedDay;

          final selectedDay = state.selectedDay;

          final titleAppbar =
              'Tháng ${DateFormat('MM - yyyy').format(focusedDay)}';

          final showTodayBtn =
              context.watch<MonthlyCalendarCubit>().showTodayBtn;

          return Scaffold(
            appBar: AppBar(
              leading: showTodayBtn ? const _TodayButton() : null,
              leadingWidth: 100,
              centerTitle: true,
              title: DateSelectorButton(
                selectedDate: selectedDay,
                onTodayBtnTap: () {
                  final now = DateTime.now();
                  context
                      .read<MonthlyCalendarCubit>()
                      .updateFocusedAndSelectedDay(now);
                },
                onSelectBtnTap: (DateTime newDate) {
                  context
                      .read<MonthlyCalendarCubit>()
                      .updateFocusedAndSelectedDay(newDate);
                },
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Text(
                      titleAppbar,
                      style: const TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                    const Icon(
                      Icons.expand_more,
                    ),
                  ],
                ),
              ),
              actions: const [
                Icon(Icons.calendar_month_outlined),
                SizedBox(width: Dimensions.spacing16)
              ],
            ),
            body: const _BodyView(),
          );
        },
      ),
    );
  }
}

class _TodayButton extends StatelessWidget {
  const _TodayButton();

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<MonthlyCalendarCubit, MonthlyCalendarState>(
      builder: (context, state) {
        return TextButton(
          onPressed: () {
            final now = DateTime.now();
            context
                .read<MonthlyCalendarCubit>()
                .updateFocusedAndSelectedDay(now);
          },
          child: Text(
            AppText.todayBtn,
            style: TextStyle(color: Colors.blue.shade900),
          ),
        );
      },
    );
  }
}

class _BodyView extends StatelessWidget {
  const _BodyView();

  @override
  Widget build(BuildContext context) {
    CalendarStyle calendarStyle = const CalendarStyle(
      cellMargin: EdgeInsets.zero,
    );

    DaysOfWeekStyle daysOfWeekStyle = DaysOfWeekStyle(
      decoration: BoxDecoration(
        color: Colors.blue.shade100,
      ),
    );

    return BlocBuilder<MonthlyCalendarCubit, MonthlyCalendarState>(
      builder: (context, state) {
        final focusedDay =
            context.watch<MonthlyCalendarCubit>().state.focusedDay;
        final selectedDay =
            context.watch<MonthlyCalendarCubit>().state.selectedDay;

        return TableCalendar(
          focusedDay: focusedDay,
          firstDay: DateTime(1000),
          lastDay: DateTime(3000),
          locale: 'vi',
          headerVisible: false,
          daysOfWeekHeight: 50,
          rowHeight: 80,
          weekendDays: const [DateTime.sunday],
          startingDayOfWeek: StartingDayOfWeek.monday,
          daysOfWeekStyle: daysOfWeekStyle,
          calendarStyle: calendarStyle,
          calendarBuilders: CalendarBuilders(
            selectedBuilder: _selectedBuilder,
            todayBuilder: _todayBuilder,
            outsideBuilder: _outsideBuilder,
            defaultBuilder: _defaultBuilder,
            dowBuilder: _dowBuilder,
          ),
          selectedDayPredicate: (day) {
            return isSameDay(selectedDay, day);
          },
          onDaySelected: (selectedDay, focusedDay) {
            context.read<MonthlyCalendarCubit>().updateFocusedDay(focusedDay);
            context.read<MonthlyCalendarCubit>().updateSelectedDay(selectedDay);

            Navigator.of(context).push(
              MaterialPageRoute(
                builder: (context) => DateCalendarDetailPage(
                  selectedDay: selectedDay,
                ),
              ),
            );
          },
          onPageChanged: (focusedDay) {
            context.read<MonthlyCalendarCubit>().updateFocusedDay(focusedDay);
          },
        );
      },
    );
  }

  Widget? _dowBuilder(BuildContext context, DateTime day) {
    final weekdayNames = {
      1: 'Hai',
      2: 'Ba',
      3: 'Tư',
      4: 'Năm',
      5: 'Sáu',
      6: 'Bảy',
      7: 'C.N',
    };

    return Center(
      child: Text(
        weekdayNames[day.weekday] ?? '',
        style: TextStyle(
          color: day.isSunday ? Colors.red.shade600 : Colors.black,
        ),
        textAlign: TextAlign.center,
      ),
    );
  }

  Widget? _outsideBuilder(
    BuildContext context,
    DateTime day,
    DateTime focusedDay,
  ) {
    return CellWidget(
      day: day,
      focusedDay: focusedDay,
      isOutsideDays: true,
      backgroundColor: Colors.blue.shade100,
      foregroundColor: Colors.blue.shade900.withOpacity(0.5),
    );
  }

  Widget? _defaultBuilder(
    BuildContext context,
    DateTime day,
    DateTime focusedDay,
  ) {
    return CellWidget(
      day: day,
      focusedDay: focusedDay,
      backgroundColor: Colors.blue.shade100,
      foregroundColor: Colors.blue.shade900,
    );
  }

  Widget? _todayBuilder(
    BuildContext context,
    DateTime day,
    DateTime focusedDay,
  ) {
    return CellWidget(
      day: day,
      focusedDay: focusedDay,
      backgroundColor: Colors.purple.shade100,
      foregroundColor: Colors.blue.shade900,
    );
  }

  Widget? _selectedBuilder(
    BuildContext context,
    DateTime day,
    DateTime focusedDay,
  ) {
    return CellWidget(
      day: day,
      focusedDay: focusedDay,
      backgroundColor: Colors.teal.shade400,
      foregroundColor: Colors.blue.shade900,
    );
  }
}
