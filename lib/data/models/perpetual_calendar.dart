import '../../presentation/utils/lunar_solar_calendar_utils.dart';
import '../dummy_data.dart' as dummy_data;
import 'daily_quotes.dart';
import 'zodiac_hour.dart';

class PerpetualCalendar {
  final DateTime solarDate;

  PerpetualCalendar(this.solarDate);

  final _lunarSolarCalendar = LunarSolarCalendarUtils();

  List<num> get lunarInfo => _lunarSolarCalendar.convertSolar2Lunar(
        solarDate.day,
        solarDate.month,
        solarDate.year,
        7,
      );

  DateTime get lunarDate => DateTime(
        lunarInfo[2].toInt(),
        lunarInfo[1].toInt(),
        lunarInfo[0].toInt(),
      );

  num get jd => _lunarSolarCalendar.jdFromDate(
        solarDate.day,
        solarDate.month,
        solarDate.year,
      );

  num get jdn => _lunarSolarCalendar.jdn(
        solarDate.day,
        solarDate.month,
        solarDate.year,
      );

  String get lunarHourByText => _lunarSolarCalendar.getBeginHour(jd);

  String get lunarDayByText => _lunarSolarCalendar.getCanDay(jdn);

  String get lunarMonthByText => _lunarSolarCalendar.getCanChiMonth(
        lunarDate.month,
        lunarDate.year,
      );
  String get lunarYearByText => _lunarSolarCalendar.getCanChiYear(
        lunarDate.year,
      );

  DailyQuotes get dailyQuotes =>
      dummy_data.dailyQuotesList[solarDate.weekday - 1];

  List<ZodiacHour> get zodiacHourList {
    return lunarDate.day % 2 == 0
        ? dummy_data.zodiacHourList.sublist(0, 6)
        : dummy_data.zodiacHourList.reversed.toList().sublist(0, 6);
  }

  List<String> get departureDirectionList {
    return lunarDate.day % 2 == 0
        ? dummy_data.departureDirectionList.sublist(0, 3)
        : dummy_data.departureDirectionList.reversed.toList().sublist(0, 3);
  }

  List<String> get goodStarList {
    return lunarDate.day % 2 == 0
        ? dummy_data.goodStarList.sublist(0, 3)
        : dummy_data.goodStarList.reversed.toList().sublist(0, 3);
  }

  List<String> get badStarList {
    return lunarDate.day % 2 == 0
        ? dummy_data.badStarList.sublist(0, 3)
        : dummy_data.badStarList.reversed.toList().sublist(0, 3);
  }

  String get thingsToDo {
    return lunarDate.day % 2 == 0
        ? dummy_data.thingsToDoList.first
        : dummy_data.thingsToDoList.last;
  }

  String get thingsNotToDo {
    return lunarDate.day % 2 == 0
        ? dummy_data.thingsNotToDoList.first
        : dummy_data.thingsNotToDoList.last;
  }
}
