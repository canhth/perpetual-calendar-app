class DailyQuotes {
  final String content;
  final String? author;
  final String backgroundImage;

  const DailyQuotes({
    required this.content,
    this.author,
    required this.backgroundImage,
  });
}
