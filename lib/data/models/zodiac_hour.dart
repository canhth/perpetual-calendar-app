class ZodiacHour {
  final String imgPath;
  final String zodiacHourText;
  final int hourStarts;
  final int hourEnds;

  const ZodiacHour({
    required this.imgPath,
    required this.zodiacHourText,
    required this.hourStarts,
    required this.hourEnds,
  });
}
