import '../gen/assets.gen.dart';
import 'models/daily_quotes.dart';
import 'models/zodiac_hour.dart';

List<DailyQuotes> dailyQuotesList = [
  DailyQuotes(
    content: 'Hãy sống mỗi ngày như thể đó là ngày cuối cùng của bạn.',
    backgroundImage: Assets.images.bg1.path,
  ),
  DailyQuotes(
    content:
        'Chúng ta có thể gặp nhiều thất bại nhưng chúng ta không được bị đánh bại.',
    author: 'Maya Angelou',
    backgroundImage: Assets.images.bg2.path,
  ),
  DailyQuotes(
    content: 'Người tuấn kiệt mới biết việc đời.',
    backgroundImage: Assets.images.bg3.path,
  ),
  DailyQuotes(
    content: 'Một tia nắng duy nhất cũng đủ để xua đuổi nhiều bóng tối',
    author: 'Francis of Assisi',
    backgroundImage: Assets.images.bg4.path,
  ),
  DailyQuotes(
    content: 'Hôm nay bạn ngồi dưới bóng mát vì có người trồng cây lâu lắm rồi',
    author: 'Warren Buffett',
    backgroundImage: Assets.images.bg5.path,
  ),
  DailyQuotes(
    content:
        'Mọi việc trở nên tốt đẹp hơn hay tồi tệ đi đều bắt nguồn từ suy nghĩ của bạn.',
    backgroundImage: Assets.images.bg6.path,
  ),
  DailyQuotes(
    content:
        'Anh hùng không kể công nhiều hay ít. Tài trí không kể mưu ít hay nhiều.',
    backgroundImage: Assets.images.bg7.path,
  ),
];

List<ZodiacHour> zodiacHourList = [
  ZodiacHour(
    imgPath: Assets.icons.zodiac.rat.path,
    zodiacHourText: 'Bính Tí',
    hourStarts: 23,
    hourEnds: 1,
  ),
  ZodiacHour(
    imgPath: Assets.icons.zodiac.ox.path,
    zodiacHourText: 'Đinh Sửu',
    hourStarts: 1,
    hourEnds: 3,
  ),
  ZodiacHour(
    imgPath: Assets.icons.zodiac.tiger.path,
    zodiacHourText: 'Bính dần',
    hourStarts: 3,
    hourEnds: 5,
  ),
  ZodiacHour(
    imgPath: Assets.icons.zodiac.cat.path,
    zodiacHourText: 'Đinh Mão',
    hourStarts: 5,
    hourEnds: 7,
  ),
  ZodiacHour(
    imgPath: Assets.icons.zodiac.dragon.path,
    zodiacHourText: 'Canh Thìn',
    hourStarts: 7,
    hourEnds: 9,
  ),
  ZodiacHour(
    imgPath: Assets.icons.zodiac.snake.path,
    zodiacHourText: 'Kỷ Tỵ',
    hourStarts: 9,
    hourEnds: 11,
  ),
  ZodiacHour(
    imgPath: Assets.icons.zodiac.horse.path,
    zodiacHourText: 'Nhâm Ngọ',
    hourStarts: 11,
    hourEnds: 13,
  ),
  ZodiacHour(
    imgPath: Assets.icons.zodiac.goat.path,
    zodiacHourText: 'Quý Mùi',
    hourStarts: 13,
    hourEnds: 15,
  ),
  ZodiacHour(
    imgPath: Assets.icons.zodiac.monkey.path,
    zodiacHourText: 'Nhâm Thân',
    hourStarts: 15,
    hourEnds: 17,
  ),
  ZodiacHour(
    imgPath: Assets.icons.zodiac.rooster.path,
    zodiacHourText: 'Kỷ Dậu',
    hourStarts: 17,
    hourEnds: 19,
  ),
  ZodiacHour(
    imgPath: Assets.icons.zodiac.dog.path,
    zodiacHourText: 'Bính Tuất',
    hourStarts: 19,
    hourEnds: 21,
  ),
  ZodiacHour(
    imgPath: Assets.icons.zodiac.pig.path,
    zodiacHourText: 'Đinh Hợi',
    hourStarts: 21,
    hourEnds: 23,
  ),
];

List<String> departureDirectionList = [
  'Hỷ Thần Tây Bắc',
  'Tài Thần Đông Nam',
  'Kê Thần (Lên Trời)',
  'Hỷ Thần Đông Nam',
  'Tài Thần Chính Nam',
  'Kê Thần Đông Bắc',
];

List<String> goodStarList = [
  'Thiên tài: Tốt cho việc cầu tài lộc; khai trương',
  'Kính Tâm: Tốt đối với an táng',
  'Tam Hợp: Tốt mọi việc',
  'Địa tài: Tốt cho việc cầu tài lộc; khai trương',
  'Nguyệt giải: Tốt mọi việc',
  'Phổ hộ (Hội hộ): Tốt mọi việc, cưới hỏi; xuất hành',
  'Lục Hợp: Tốt mọi việc',
  'Kim đường: Hoàng Đạo - Tốt mọi việc',
];

List<String> badStarList = [
  'Đại Hao (Tử Khí, Quan Phú): Xấu mọi việc',
  'Nguyệt Yếm đại họa: Xấu đối với xuất hành, giá thú',
  'Vãng vong (Thổ kỵ): Kỵ xuất hành; cưới hỏi; cầu tài lộc; khởi công, động thổ',
  'Hoang vu: Xấu mọi việc',
  'Trùng Tang: Kỵ cưới hỏi; an táng; khởi công, động thổ, xây dựng nhà cửa',
  'Trùng phục: Kỵ giá thú; an táng',
  'Ly sàng: Kỵ cưới hỏi',
];

List<String> thingsToDoList = [
  'Khởi công mọi việc đều tốt. Tốt nhất là dựng cột, cất lầu, làm dàn gác, cưới gả, trổ cửa, dựng cửa, tháo nước hay các việc liên quan đến thủy lợi, cắt áo.',
  'Tốt cho các việc giao dịch, buôn bán, làm chuồng lục súc, thi ơn huệ',
];

List<String> thingsNotToDoList = [
  'Đóng giường, lót giường, đi đường thủy.',
  'Xấu cho các việc xuất hành, thưa kiện, châm chích, an sàng',
];
