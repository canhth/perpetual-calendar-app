/// GENERATED CODE - DO NOT MODIFY BY HAND
/// *****************************************************
///  FlutterGen
/// *****************************************************

// coverage:ignore-file
// ignore_for_file: type=lint
// ignore_for_file: directives_ordering,unnecessary_import,implicit_dynamic_list_literal,deprecated_member_use

import 'package:flutter/widgets.dart';

class $AssetsIconsGen {
  const $AssetsIconsGen();

  $AssetsIconsZodiacGen get zodiac => const $AssetsIconsZodiacGen();
}

class $AssetsImagesGen {
  const $AssetsImagesGen();

  /// File path: assets/images/app_logo.png
  AssetGenImage get appLogo =>
      const AssetGenImage('assets/images/app_logo.png');

  /// File path: assets/images/app_logo_play_store_512.png
  AssetGenImage get appLogoPlayStore512 =>
      const AssetGenImage('assets/images/app_logo_play_store_512.png');

  /// File path: assets/images/bg1.jpg
  AssetGenImage get bg1 => const AssetGenImage('assets/images/bg1.jpg');

  /// File path: assets/images/bg2.jpg
  AssetGenImage get bg2 => const AssetGenImage('assets/images/bg2.jpg');

  /// File path: assets/images/bg3.jpg
  AssetGenImage get bg3 => const AssetGenImage('assets/images/bg3.jpg');

  /// File path: assets/images/bg4.jpg
  AssetGenImage get bg4 => const AssetGenImage('assets/images/bg4.jpg');

  /// File path: assets/images/bg5.jpg
  AssetGenImage get bg5 => const AssetGenImage('assets/images/bg5.jpg');

  /// File path: assets/images/bg6.jpg
  AssetGenImage get bg6 => const AssetGenImage('assets/images/bg6.jpg');

  /// File path: assets/images/bg7.jpg
  AssetGenImage get bg7 => const AssetGenImage('assets/images/bg7.jpg');

  /// List of all assets
  List<AssetGenImage> get values =>
      [appLogo, appLogoPlayStore512, bg1, bg2, bg3, bg4, bg5, bg6, bg7];
}

class $AssetsIconsZodiacGen {
  const $AssetsIconsZodiacGen();

  /// File path: assets/icons/zodiac/cat.png
  AssetGenImage get cat => const AssetGenImage('assets/icons/zodiac/cat.png');

  /// File path: assets/icons/zodiac/dog.png
  AssetGenImage get dog => const AssetGenImage('assets/icons/zodiac/dog.png');

  /// File path: assets/icons/zodiac/dragon.png
  AssetGenImage get dragon =>
      const AssetGenImage('assets/icons/zodiac/dragon.png');

  /// File path: assets/icons/zodiac/goat.png
  AssetGenImage get goat => const AssetGenImage('assets/icons/zodiac/goat.png');

  /// File path: assets/icons/zodiac/horse.png
  AssetGenImage get horse =>
      const AssetGenImage('assets/icons/zodiac/horse.png');

  /// File path: assets/icons/zodiac/monkey.png
  AssetGenImage get monkey =>
      const AssetGenImage('assets/icons/zodiac/monkey.png');

  /// File path: assets/icons/zodiac/ox.png
  AssetGenImage get ox => const AssetGenImage('assets/icons/zodiac/ox.png');

  /// File path: assets/icons/zodiac/pig.png
  AssetGenImage get pig => const AssetGenImage('assets/icons/zodiac/pig.png');

  /// File path: assets/icons/zodiac/rabit.png
  AssetGenImage get rabit =>
      const AssetGenImage('assets/icons/zodiac/rabit.png');

  /// File path: assets/icons/zodiac/rat.png
  AssetGenImage get rat => const AssetGenImage('assets/icons/zodiac/rat.png');

  /// File path: assets/icons/zodiac/rooster.png
  AssetGenImage get rooster =>
      const AssetGenImage('assets/icons/zodiac/rooster.png');

  /// File path: assets/icons/zodiac/snake.png
  AssetGenImage get snake =>
      const AssetGenImage('assets/icons/zodiac/snake.png');

  /// File path: assets/icons/zodiac/tiger.png
  AssetGenImage get tiger =>
      const AssetGenImage('assets/icons/zodiac/tiger.png');

  /// List of all assets
  List<AssetGenImage> get values => [
        cat,
        dog,
        dragon,
        goat,
        horse,
        monkey,
        ox,
        pig,
        rabit,
        rat,
        rooster,
        snake,
        tiger
      ];
}

class Assets {
  Assets._();

  static const $AssetsIconsGen icons = $AssetsIconsGen();
  static const $AssetsImagesGen images = $AssetsImagesGen();
}

class AssetGenImage {
  const AssetGenImage(this._assetName);

  final String _assetName;

  Image image({
    Key? key,
    AssetBundle? bundle,
    ImageFrameBuilder? frameBuilder,
    ImageErrorWidgetBuilder? errorBuilder,
    String? semanticLabel,
    bool excludeFromSemantics = false,
    double? scale,
    double? width,
    double? height,
    Color? color,
    Animation<double>? opacity,
    BlendMode? colorBlendMode,
    BoxFit? fit,
    AlignmentGeometry alignment = Alignment.center,
    ImageRepeat repeat = ImageRepeat.noRepeat,
    Rect? centerSlice,
    bool matchTextDirection = false,
    bool gaplessPlayback = false,
    bool isAntiAlias = false,
    String? package,
    FilterQuality filterQuality = FilterQuality.low,
    int? cacheWidth,
    int? cacheHeight,
  }) {
    return Image.asset(
      _assetName,
      key: key,
      bundle: bundle,
      frameBuilder: frameBuilder,
      errorBuilder: errorBuilder,
      semanticLabel: semanticLabel,
      excludeFromSemantics: excludeFromSemantics,
      scale: scale,
      width: width,
      height: height,
      color: color,
      opacity: opacity,
      colorBlendMode: colorBlendMode,
      fit: fit,
      alignment: alignment,
      repeat: repeat,
      centerSlice: centerSlice,
      matchTextDirection: matchTextDirection,
      gaplessPlayback: gaplessPlayback,
      isAntiAlias: isAntiAlias,
      package: package,
      filterQuality: filterQuality,
      cacheWidth: cacheWidth,
      cacheHeight: cacheHeight,
    );
  }

  ImageProvider provider() => AssetImage(_assetName);

  String get path => _assetName;

  String get keyName => _assetName;
}
