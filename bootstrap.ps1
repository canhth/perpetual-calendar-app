# Check the enforcement policy for the current user account
$currentPolicy = Get-ExecutionPolicy -Scope CurrentUser

# If the enforcement policy is not "Unrestricted"
if ($currentPolicy -ne "Unrestricted") {
    # Set the execution policy for the current user account to "Unrestricted"
    Set-ExecutionPolicy -Scope CurrentUser -ExecutionPolicy Unrestricted
}

Write-Host "Bootstrap started."

flutter clean
flutter pub get
flutter packages pub run build_runner build --delete-conflicting-outputs

Write-Host "Bootstrap finished."